package com.classpath.orderapi.controller;

import com.classpath.orderapi.model.Order;
import com.classpath.orderapi.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders/")
@AllArgsConstructor
public class OrderRestController {

    private OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchOrders();

    }

    @GetMapping("/{orderId}")
    public Order fetchOrderById(@PathVariable("orderId") long orderId){
        return  this.orderService.findOrderById(orderId);
    }

    @PostMapping
    public Order saveOrder(@RequestBody Order order){
        return  this.orderService.saveOrder(order);
    }

    @DeleteMapping("/orderId")
    public void deleteOrder(@PathVariable("orderId") long orderId) {
        this.orderService.deleteOrderById(orderId);
    }
}