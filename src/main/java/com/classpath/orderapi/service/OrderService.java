package com.classpath.orderapi.service;

import com.classpath.orderapi.model.Order;

import java.util.Set;

public interface OrderService {

    Order saveOrder(Order order);

    Set<Order> fetchOrders();

    Order findOrderById(long orderId);

    void deleteOrderById(long orderId);
}