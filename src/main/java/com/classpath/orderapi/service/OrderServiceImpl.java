package com.classpath.orderapi.service;

import com.classpath.orderapi.model.Order;
import com.classpath.orderapi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Override
    public Order saveOrder(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet(this.orderRepository.findAll());
    }

    @Override
    public Order findOrderById(long orderId) {
        return this.orderRepository.findById(orderId).orElseThrow( () -> new IllegalArgumentException("Invalid order Id"));
    }

    @Override
    public void deleteOrderById(long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}